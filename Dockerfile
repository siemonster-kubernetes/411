FROM php:7-apache

RUN apt-get update && \
    apt-get -y install \
    unzip \
    git \
    libxml2-dev \
    libcurl4-openssl-dev \
    sqlite3 \
    libsqlite3-dev \
    cron \
    ssmtp \
    supervisor \
    mysql-client \
    curl \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* 
# add SSMTP
COPY     ./ssmtp.conf /etc/ssmtp/ssmtp.conf
COPY     ./php-smtp.ini /usr/local/etc/php/conf.d/php-smtp.ini
COPY     ./init.sh /root/init.sh

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/bin --filename=composer
RUN docker-php-ext-configure \
    pdo_mysql --with-pdo-mysql=mysqlnd
RUN docker-php-ext-install \
    xml \
    pdo_mysql \
    pdo_sqlite \
    mbstring \
    curl \
    pcntl

RUN a2enmod headers rewrite

WORKDIR /var/www/411
RUN curl -L https://github.com/siemonster/misc/raw/master/release-6.1.tgz | tar xz

#RUN composer install --no-dev --optimize-autoloader
RUN COMPOSER=composer-es6x.json composer install --no-dev --optimize-autoloader
RUN mkdir /data
COPY 411.conf /etc/apache2/sites-available/000-default.conf
COPY config.php /data/config.php
RUN ln -sf /data/config.php config.php

RUN chmod ugo+rx -R /data
RUN chown www-data:www-data -R /data
RUN mkdir -p /.backup/data
COPY ./config.php /.backup/data/

VOLUME /data
COPY supervisord.conf /root/supervisord.conf
COPY 411_cron /etc/cron.d/
RUN chmod 644 /etc/cron.d/411_cron  \
    && touch /etc/crontab /etc/cron.*/*

#Environment
ENV ELASTIC_PWD changeme
ENV MYSQL_DATABASE changeme
EXPOSE 80
CMD ["/usr/bin/supervisord", "-c", "/root/supervisord.conf"]
